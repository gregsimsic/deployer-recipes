<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'https://gregsimsic@bitbucket.org/gregsimsic/205-hudson-craft-site.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('project.com')
    ->set('deploy_path', '~/{{application}}');    
    

// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info', // echo info block
    'deploy:prepare', // prepare for atomic releases
    'deploy:lock', // create .lock file to prevent use
    'deploy:release', // Clean up unfinished releases and prepare next release
    'deploy:update_code', // git clone & checkout branch
    'deploy:shared', // manage symlinks to shared files among releases
    'deploy:writable', // chmod on dirs after git pull
    'deploy:vendors', // composer install (action can be changed)
    'deploy:clear_paths', // delete specified dirs
    'deploy:symlink', // set symlink to new release
    'deploy:unlock', // delete .lock file
    'cleanup', // Cleaning up old releases
    'success' // echo success
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
