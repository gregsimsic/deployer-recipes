<?php
/*
 *
 * CRAFT 3 DEPLOY RECIPE
 *
 *
 */

namespace Deployer;

require 'recipe/common.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/db.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/craft.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/sync.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/file.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/git.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/composer.php';
require 'vendor/gregsimsic/deployer-recipes/lib/Utils.php';

/**
 *  CONFIG
 *
 */

// read hosts from config
inventory('deploy.yml');

// Project name
set('application', 'Very Fine Site');

// site repository -- NOTE: must be SSH
set('repository', 'git@bitbucket.org:gregsimsic/veryfine-site.git');

set('default_stage', 'stage');

// Files & directories removed after git pull on remote
set('remove_items', [
    '.babelrc',
    '.env.example',
    'deploy.php',
    'deploy.yml',
    'deploy.example.yml',
    'gulpfile.babel.js',
    'package.json',
    'README.md',
    'setup'
]);

// Writable dirs by web server
set('writable_chmod_mode', 755);
set('writable_dirs', [
    'storage',
    'web'
]);

// The list of directories given as options to the sync task -- no trailing slashes
set('sync_items', [
    'web/assets',
    'templates'
]);

// Tasks
desc('Deploy the site');
task('deploy', [
    'deploy:info', // echo info block
    'git:run', // clone or pull
    'composer:run', // composer install -- updates handled by CMS
    'file:remove', // delete specified items that are under version control, or not ???
    'dir:writables',
    'craft:clear-caches', // all
//    'sync:up', // choose non version controlled files & dirs to upload: assets, media
    'success' // echo success
]);

desc('Test Task');
task('test', function () {

    writeln( 'Test task is working' );

});
