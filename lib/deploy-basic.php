<?php
/*
 *
 * BASIC DEPLOY RECIPE -- no git, non-atomic, just sync a list of files and dirs to a server
 *
 *
 */

namespace Deployer;

require 'recipe/common.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/sync.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/file.php';

/**
 *  CONFIG
 *
 */

// read hosts from config
inventory('deploy.yml');

set('application', 'APP_NAME');

set('default_stage', 'stage');

// The list of directories to be synced
set('sync_items', [
    'web/assets',
    'web/index.html'
]);

// Tasks
desc('Deploy the site');
task('deploy', [
    'deploy:info', // echo info block
    'sync:all', // sync all 'sync_items'
    'success' // echo success
]);

desc('Test Task');
task('test', function () {

    writeln( 'Test task is working' );

});