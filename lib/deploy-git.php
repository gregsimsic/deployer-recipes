<?php
/*
 *
 * BASIC DEPLOY RECIPE -- no git, non-atomic, just sync a list of files and dirs to a server
 *
 *
 */

namespace Deployer;

require 'recipe/common.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/sync.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/file.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/git.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/scratch.php';

/**
 *  CONFIG
 *
 */

// read hosts from config
inventory('deploy.yml');

// Project name
set('application', 'Very Fine Site');

// site repository
set('repository', 'https://gregsimsic@bitbucket.org/gregsimsic/XXXX.git');

set('default_stage', 'stage');

// The list of directories & files to be deleted after git pull
set('remove_items', [
    'deploy.php'
]);

// The list of non version controlled files & dirs to be synced after git pull
set('sync_items', [
    'web/test',
]);

// Tasks
desc('Deploy the site');
task('deploy', [
    'deploy:info', // echo info block
    'git:run', // clone or pull
    'file:remove', // delete specified items
    'sync:up', // choose non version controlled files & dirs to upload
    'success' // echo success
]);

desc('Test Task');
task('test', function () {

    writeln( 'Test task is working' );

});
