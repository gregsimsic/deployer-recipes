<?php
// https://gist.github.com/mtwalsh/fce3c4aa416996e5900e8ac9f471dd6c

namespace Deployer;
require 'recipe/common.php';

// Project name
set('application', 'enovate.co.uk');

// Project repository
set('repository', 'git@githosting.com:enovatedesign/project.git');

// Shared files/dirs between deploys
set('shared_files', [
    '.env'
]);
set('shared_dirs', [
    'storage'
]);

// Writable dirs by web server
set('writable_dirs', [
    'storage',
    'storage/runtime',
    'storage/logs',
    'storage/rebrand',
    'public/cpresources'
]);

// Set the worker process user
set('http_user', 'worker');

// Set the default deploy environment to production
set('default_stage', 'production');

// Disable multiplexing
set('ssh_multiplexing', false);

// Tasks

// Upload build assets
task('upload', function () {
    upload(__DIR__ . "/public/assets/", '{{release_path}}/public/assets/');
    //upload(__DIR__ . "/public/service-worker.js", '{{release_path}}/public/service-worker.js');
});

desc('Execute migrations');
task('craft:migrate', function () {
    run('{{release_path}}/craft migrate/up');
})->once();

// Hosts

// Production Server(s)
host('110.164.16.59', '110.164.16.34', '110.164.16.50')
    ->set('deploy_path', '/websites/{{application}}')
    ->set('branch', 'master')
    ->stage('production')
    ->user('someuser');

// Staging Server
host('192.168.16.59')
    ->set('deploy_path', '/websites/{{application}}')
    ->set('branch', 'develop')
    ->stage('staging')
    ->user('someuser');

// Group tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'upload', // Custom task to upload build assets
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] Run migrations
after('deploy:vendors', 'craft:migrate');

// [Optional] If deploy fails automatically unlock
after('deploy:failed', 'deploy:unlock');

// Run with '--parallel'
// dep deploy --parallel