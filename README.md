# Deployer Recipes

## Features

* Deploy site (atomic and non)
* Sync uploads
* Pull/push MySql databases to/from remotes and local

## Requirements

* [Deployer PHP](http://deployer.org/)

## Installation

run `composer require gregsimsic/deployer-recipes`.

Make sure to include recipe files in your `deploy.php`:

    require 'vendor/gregsimsic/deployer-recipes/recipes/db.php';

## Configuration
Edit the deploy.yml. See examples in /lib/yamls.

## Available tasks

Push database : `dep db:push prod`
Pull database : `dep db:pull prod`

Rsync files up--choose from list: `dep sync:up`
Rsync files down--choose from list: `dep sync:down`
Rsync all files up: `dep sync:down`

## References

#### Craft 
* https://gist.github.com/mtwalsh/fce3c4aa416996e5900e8ac9f471dd6c
* https://www.enovate.co.uk/blog/2018/07/23/atomic-deployment-with-deployer

#### Laravel
* https://medium.com/@nickdenardis/zero-downtime-local-build-laravel-5-deploys-with-deployer-a152f0a1411f

#### General
* https://antonydandrea.com/deploying-with-deployer/
* https://medium.com/@Alibaba_Cloud/automating-your-php-application-deployment-with-php-deployer-b00c1dc64d4a
https://codereviewvideos.com/course/deployer-easy-php-deployment/video/running-your-first-deployer-deploy

#### Shared server deployment
* https://discourse.roots.io/t/heres-deployer-recipes-to-deploy-bedrock/9896
* https://github.com/FlorianMoser/plesk-deployer

#### Wordpress recipes
* https://github.com/cstaelen/deployer-wp-recipes
* https://github.com/danielroe/deployer-wp-recipes
 