<?php

/**
 * Deployer Craft recipes
 *
 */

namespace Deployer;

desc('Make Craft storage directory');
task('craft:mk-storage', function () {

    run("mkdir {{deploy_path}}/storage");

});

desc('Clear Craft caches');
task('craft:clear-caches', function () {

    writeln('<comment>Clearing Craft caches...</comment>');

    run("cd {{deploy_path}} && ./craft clear-caches/all");

    writeln('<comment>Caches cleared.</comment>');

});

desc('Set Craft permissions');
task('craft:perms', function () {

    writeln('<comment>Setting Craft permissions...</comment>');

    cd('{{deploy_path}}');

    // TODO: set Craft permissions
    run("chmod {{writable_chmod_mode}} $dirs");

    writeln('<comment>Permissions set.</comment>');

});
