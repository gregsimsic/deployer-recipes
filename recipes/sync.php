<?php

/**
 * Deployer recipes to sync files & folders between local & remote environments
 * and also between 2 dirs on same remote
 *
 */

namespace Deployer;

/**
 * Upload a directory -- does not delete existing files
 *
 */
desc('Upload a list of file and directories to site');
task('sync:all', function () {

    // must pick a remote host
    if ( get('hostname') === 'localhost' ) {
        throw new \Exception("No remote host specified");
    }

    writeln('');
    writeln('<comment>Uploading all to ' . get('stage') . '</comment>');
    writeln('');

    foreach (get('sync_items') as $item) {
        writeln('uploading: '. $item);
        $destination = $item;
        $source = (is_dir($item)) ? $item  . '/' : $item;
        upload($source, '{{deploy_path}}/' . $destination );
    }

});

/**
 * Upload a directory -- does not delete existing files
 *
 */
desc('Upload a directory from a list of choices set in deploy.php-- does not delete existing files');
task('sync:up', function () {

    // must pick a remote host
    if ( get('hostname') === 'localhost' ) {
        throw new \Exception("No remote host specified");
    }

    writeln('');
    writeln('<comment>Uploading to ' . get('stage') . '</comment>');
    writeln('');

    $item = askChoice('Choose a directory to upload:', get('sync_items'));

    $destination = $item;
    $source = (is_dir($item)) ? $item  . '/' : $item;
    upload($source, '{{deploy_path}}/' . $destination );
});

/**
 * Download a directory -- does not delete existing files
 *
 */
desc('Download a directory from a list of choices set in deploy.php-- does not delete existing files');
task('sync:down', function () {

    // must pick a remote host
    if ( get('hostname') === 'localhost' ) {
        throw new \Exception("No remote host specified");
    }

    writeln('');
    writeln('<comment>Downloading from ' . get('stage') . '</comment>');
    writeln('');

    $item = askChoice('Choose a directory to download:', get('sync_items'));

    $destination = $item;
    $source = (is_dir($item)) ? $item  . '/' : $item;
    download('{{deploy_path}}/' . $source, $destination );
});

/**
 *
 * Sync a directory between remote sites on the same server with --delete flag
 *
 * TODO: this could be updated to work across 2 servers since the command is being run on the remote host
 *
 */
desc('Sync a directory between remote sites on the same server with --delete flag');
task('sync:remotes', function () {

    $hosts = Deployer::get()->hosts;

    $remoteHostsByName = [];
    $remoteHostNames = [];

    foreach ($hosts as $host) {
        // exclude local hosts
        if($host->getHostname() !== 'local') {
            $remoteHostsByName[$host->getHostname()] = $host;
            $remoteHostNames[] = $host->getHostname();
        }
    }

    $from = askChoice('Sync from:', $remoteHostNames);

    // remove 'from' from the choices
    if (($key = array_search($from, $remoteHostNames)) !== false) {
        unset($remoteHostNames[$key]);
    }

    $to = askChoice('Sync to:', $remoteHostNames);

    $toPath = host($to)->get('deploy_path');

    $item = askChoice('Select a directory:', get('sync_items'));

    $confirm = askConfirmation('Copy '. $item . ' from ' . $from . ' to ' . $to . '?');

    if ($confirm) {

        // run command on source($from) host
        on( host($from), function ($host) use ($toPath, $item) {

            $destination = $item;
            $source = (is_dir($item)) ? $item  . '/' : $item;

//            writeln('rsync -a --delete ' . get('deploy_path') . '/' . $item . '/ ' . $toPath . '/' . $item . '');
            run('rsync -a --delete ' . get('deploy_path') . '/' . $source . ' ' . $toPath . '/' . $destination . '');

        });
    } else {
        writeln("Perhaps that's best.");
    }

});