<?php

namespace Deployer;

/**
 * Get current git HEAD branch as default branch to deploy.
 */
set('branch', function () {
    try {
        $branch = runLocally('git rev-parse --abbrev-ref HEAD');
    } catch (\Throwable $exception) {
        $branch = null;
    }

    if ($branch === 'HEAD') {
        $branch = null; // Travis-CI fix
    }

    if (input()->hasOption('branch') && !empty(input()->getOption('branch'))) {
        $branch = input()->getOption('branch');
    }

    return $branch;
});

task('git:exists', function () {
    try {
        $test = run('cd {{deploy_path}} && git rev-parse --is-inside-work-tree');
    } catch (\Throwable $exception) {
        $test = 'failed: no repo found';
    }

    writeln($test);

});
task('git:status', function () {
    try {
        $test = run('cd {{deploy_path}} && git status');
    } catch (\Throwable $exception) {
        $test = 'failed: ' . $exception->getMessage();
    }

    writeln($test);

});

desc('Git Clone or Pull');
task('git:run', function () {
    $repository = trim(get('repository'));
    $branch = get('branch');
    $git = get('bin/git');
    $options = [
        'tty' => get('git_tty', false),
    ];

    $at = '';
    if (!empty($branch)) {
        $at = "-b $branch";
    }

    // If option `tag` is set
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (!empty($tag)) {
            $at = "-b $tag";
        }
    }

    // If option `tag` is not set and option `revision` is set
    if (empty($tag) && input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            $depth = '';
        }
    }

    // Enter deploy_path if present
    if (has('deploy_path')) {
        cd('{{deploy_path}}');
    }

    try {
        run("$git rev-parse --is-inside-work-tree");

        writeln("pulling from $branch");

        // clean up
        run("$git reset --hard HEAD", $options);
        $output = run("$git pull origin", $options);

        writeln($output);

    } catch (\Throwable $exception) {

        writeln("cloning from $repository");

        // clone into current dir
        $output = run("$git clone $repository . 2>&1", $options);

        // checkout matching branch
        $output .= run("$git checkout $branch 2>&1", $options);

        writeln($output);

    }

});

desc('Git pull');
task('git:pull', function () {
    $branch = get('branch');
    $git = 'git';
    $options = [
        'tty' => get('git_tty', false),
    ];

    $at = '';
    if (!empty($branch)) {
        $at = "-b $branch";
    }

    // If option `tag` is set
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (!empty($tag)) {
            $at = "-b $tag";
        }
    }

    // If option `tag` is not set and option `revision` is set
    if (empty($tag) && input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            $depth = '';
        }
    }

    // Enter deploy_path if present
    if (has('deploy_path')) {
        cd('{{deploy_path}}');
    }

    // clean up
    run("$git reset --hard HEAD", $options);

    run("$git pull origin", $options);

});
