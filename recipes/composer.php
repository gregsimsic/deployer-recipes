<?php

/**
 * Deployer general deploy recipes
 *
 */

namespace Deployer;

desc('Remove files and dirs');
task('composer:run', function () {

    // abandon if host is local (or this could be and askConfirmation() )
    if ( get('hostname') === 'localhost' ) {
        throw new \Exception("Don't delete local files!");
    }

    // install
    run('cd {{deploy_path}} && composer install --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest');

});
