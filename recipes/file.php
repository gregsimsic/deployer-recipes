<?php

/**
 * Deployer file handling recipes
 *
 */

namespace Deployer;

desc('Cleaning up files and/or directories');
task('file:remove', function () {

    writeln('<comment>Removing files...</comment>');

    $paths = get('remove_items');
    $sudo  = get('clear_use_sudo') ? 'sudo' : '';

    foreach ($paths as $path) {
        run("$sudo rm -rf {{deploy_path}}/$path");
    }

    writeln('<comment>Finished removing files</comment>');

});

desc('Make directories writable');
task('dir:writables', function () {

    try {

        writeln('<comment>Making directories writable...</comment>');

        $dirs = join(' ', get('writable_dirs'));

        cd('{{deploy_path}}');

        // Create directories if they don't exist
        run("mkdir -p $dirs");

        run("chmod {{writable_chmod_mode}} $dirs");

        writeln('<comment>Finished making directories writable.</comment>');

//        $formatter = Deployer::get()->getHelper('formatter');
//
//        $errorMessage = [
//            "Unable to setup correct permissions for writable dirs.",
//        ];
//        write($formatter->formatBlock($errorMessage, 'bg=blue;fg=white', true));


    } catch (\RuntimeException $e) {

        $formatter = Deployer::get()->getHelper('formatter');

        $errorMessage = [
            "Unable to setup correct permissions for writable dirs.",
        ];
        write($formatter->formatBlock($errorMessage, 'error', true));

        throw $e;
    }

});
